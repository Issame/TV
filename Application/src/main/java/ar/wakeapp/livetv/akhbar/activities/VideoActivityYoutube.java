package ar.wakeapp.livetv.akhbar.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

import ar.wakeapp.livetv.akhbar.R;

public class VideoActivityYoutube extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    InterstitialAd mInterstitialAd;

    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    private String stream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_youtube);

        //getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().hide();

        stream = getIntent().getExtras().getString("stream");

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(getString(R.string.youtube_api_key), this);


        // Create the interstitial.
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_inter_id));
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onInitializationSuccess(Provider provider, final YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(stream);

            mInterstitialAd.setAdListener(new AdListener(){
                public void onAdLoaded(){
                   mInterstitialAd.show();
                }
                public void onAdClosed(){
                    //player.loadVideo(stream);
                    player.play();
                }
            });
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(getString(R.string.youtube_api_key), this);
        }
    }

    protected Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

}