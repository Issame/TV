package ar.wakeapp.livetv.akhbar.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ar.wakeapp.livetv.akhbar.activities.MainActivity;
import ar.wakeapp.livetv.akhbar.activities.VideoActivityBrightcove;
//import ar.wakeapp.livetv.akhbar.activities.VideoActivityYoutube;
import ar.wakeapp.livetv.akhbar.core.Channel;
import ar.wakeapp.livetv.akhbar.core.ListViewItem;
import ar.wakeapp.livetv.akhbar.core.MyListViewAdapter;
import ar.wakeapp.livetv.akhbar.utils.RetrieveChannelsTask;

public class EnChannelListFragment extends ListFragment {
    // ListView items list
    private List<ListViewItem> mItems;
    private static ArrayList<Channel> enChannelsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if((enChannelsList == null) || (enChannelsList.size() == 0)) {
                enChannelsList = new RetrieveChannelsTask().execute("http://root.issame.net/tvapps/news_brightcove.json", "en").get();
            }

            // initialize the items list
            mItems = new ArrayList<ListViewItem>();
            Resources resources = getResources();

            for (int i = 0; i < enChannelsList.size(); i++) {
                Channel channel = enChannelsList.get(i);
                    Drawable logo = new BitmapDrawable(getResources(), channel.getLogo());
                    mItems.add(new ListViewItem(logo, channel.getTitle(), channel.getDescription()));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // initialize and set the list adapter
        setListAdapter(new MyListViewAdapter(getActivity(), mItems));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // remove the dividers from the ListView of the ListFragment
        getListView().setDivider(null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // retrieve theListView item
        ListViewItem item = mItems.get(position);

        Intent intent = new Intent(getActivity(), VideoActivityBrightcove.class);
        //Intent intent = new Intent(getActivity(), VideoActivityYoutube.class);
        intent.putExtra("stream", enChannelsList.get(position).getStream());
        startActivity(intent);
    }
}