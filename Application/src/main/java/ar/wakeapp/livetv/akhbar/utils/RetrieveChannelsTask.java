package ar.wakeapp.livetv.akhbar.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ar.wakeapp.livetv.akhbar.core.Channel;

public class RetrieveChannelsTask extends AsyncTask<String, Void, ArrayList<Channel>> {
    @Override
    protected ArrayList<Channel> doInBackground(String... strings) {
        ArrayList<Channel> channelsList = new ArrayList<Channel>();

        String json = Utils.getJSONFromUrl(strings[0]);
        String lang = strings[1];

        if (json != null) {
            try {
                JSONObject jObj = new JSONObject(json);
                JSONArray channels = (JSONArray) jObj.getJSONArray("channel");

                for (int i = 0; i < channels.length(); i++) {
                    JSONObject channel = channels.getJSONObject(i);
                    if(lang.equals(channel.getString("language"))){
                        String title = channel.getString("title");
                        String description = channel.getString("description");
                        String language = channel.getString("language");
                        Bitmap logo = Utils.LoadBitmapFromURL(channel.getString("logo"));
                        String stream = channel.getString("stream");
                        Channel c = new Channel(title, description, language, logo, stream);
                        channelsList.add(c);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return channelsList;
    }
}