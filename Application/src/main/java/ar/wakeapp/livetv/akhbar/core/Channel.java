package ar.wakeapp.livetv.akhbar.core;


import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Channel implements Parcelable {

    String title;
    String description;
    String language;
    Bitmap logo;
    String stream;

    public Channel(String title, String description, String language, Bitmap logo, String stream) {
        this.title = title;
        this.description = description;
        this.language = language;
        this.logo = logo;
        this.stream = stream;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Bitmap getLogo() {
        return logo;
    }

    public void setLogo(Bitmap logo) {
        this.logo = logo;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }




    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        //out.writeString(title);
        //out.writeString(description);
        //out.writeString(language);
        //out.writeString(logo);
        //out.writeString(stream);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
        public Channel createFromParcel(Parcel in) {
            return new Channel(in);
        }

        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Channel(Parcel in) {
        //title = in.readString();
        //description = in.readString();
        //language = in.readString();
        //logo = in.readString();
        //stream = in.readString();
    }
}
